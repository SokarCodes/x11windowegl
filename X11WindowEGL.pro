TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    X11window.cpp \
    GLESv2context.cpp
LIBS += -lX11 -lEGL -lGLESv2

HEADERS += \
    X11window.h \
    GLESv2context.h

