#include <iostream>
#include <assert.h>

// Needed for X11 server connection
#include "X11/Xlib.h"

// EGL library
#include "EGL/egl.h"

// OpenGL ES 2.0 libraries and extension
#include "GLES2/gl2.h"
#include "GLES2/gl2ext.h"

#include "X11window.h"
#include "GLESv2context.h"

int main()
{
    // Make connection to X11 server and open it.
    X11Window *xWindow = new X11Window();
    GLESv2Context *renderContext = new GLESv2Context();
    xWindow->open();
    renderContext->bindWindow(xWindow);
    renderContext->init();
    xWindow->refresh();
    sleep(5);

    delete xWindow;
    delete renderContext;

    return 0;
}

